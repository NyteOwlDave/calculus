# Calculus 101

These are my calculus notes.

# Reference

[kindle]: <https://read.amazon.com/>
[desmos]: <https://www.desmos.com/calculator>
[calc]: <https://www.calculussolution.com/calculus-calculator>

* Calculus in 5 Hours ([Kindle][kindle])
* [Desmos][desmos]
* [Calc-Calc][calc]

---
